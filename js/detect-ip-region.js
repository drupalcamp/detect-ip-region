/**
 * @file
 * JavaScript for ajax_example.
 */

(function ($, Drupal, drupalSettings) {

  // Re-enable form elements that are disabled for non-ajax situations.
  Drupal.behaviors.showHideRegionBlock = {
    attach: function () {
      // If ajax is enabled, we want to hide items that are marked as hidden in
      // our example.
  
      let region = drupalSettings.currentRegion;
      let regionPath = drupalSettings.currentRegionPath;
      // console.log("region=" + region);
      $regionSwitcher = $('.nav--utility-menu>.nav__item>.region-switcher');
      if(region && region !== 'none' && region!=="") {
        $regionSwitcher.attr("href", regionPath).html(region);
      }
  

      let $close = $('#detect-ip-region-form .close');
      let $cancel = $('#detect-ip-region-form .cancel');
      let $regeionBlock = $('#block-changemyregionblock');
      // let $mobileMenu = $('.region-mobile-navigation');
      let $switcher = $('.nav .region-switcher');

      $switcher.click(function ( event) {
        event.preventDefault();
        $regeionBlock.slideDown('slow');
        // $mobileMenu.hide();
      });

      $close.click(function (event) {
        event.preventDefault();
        $regeionBlock.slideUp('slow');
      });
      $cancel.click(function (event) {
        event.preventDefault();
        $regeionBlock.slideUp('slow');
      });
 
    }
  };

})(jQuery, Drupal, drupalSettings);
