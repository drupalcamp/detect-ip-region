<?php

namespace Drupal\detect_ip_region\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Change My Region' Block.
 *
 * @Block(
 *   id = "change_region_block",
 *   admin_label = @Translation("Change My Region Block"),
 *   category = @Translation("Custom block"),
 * )
 */
class ChangeRegionBlock extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $form = \Drupal::formBuilder()->getForm('\Drupal\detect_ip_region\Form\ChangeRegionForm');
        return $form;
    }

}
