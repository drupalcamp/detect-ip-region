<?php

namespace Drupal\Tests\detect_ip_region\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the Detect IP Region module.
 * 
 */

class DetectIPRegionTests extends BrowserTestBase {
    /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = array('detect_ip_region');


  /**
   * A simple user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;


  /**
   * Perform initial setup tasks that run before every test method.
   *
   * @return void
   */
  public function setUp() {
    parent::setUp();
    $this->user = $this-drupalCreatedUser(['administer content']);
  }

  /**
   * Tests that the Detect IP Region Form page.
   */
  public function testDeletectIPRegeionFormPageExists() {
    //login.
    $this->drupalLogin($this->user);

    // Generator test;
    $this->drupalGet('detect-ip-region/region-form-page');
    $this->assertSession()->statusCodeEquals(200);
  }

}